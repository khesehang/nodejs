module.exports = function(userPost, postData) {
    if(postData.title)
        userPost.title = postData.title
    if(postData.body)
        userPost.body = postData.body
    if(postData.photo)
        userPost.photo = postData.photo
    if(postData.likes)
        userPost.likes = postData.likes
} 