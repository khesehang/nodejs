const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../keys')
const mongoose = require('mongoose')
const UserModel = require('../models/user.model')

module.exports = (req,res,next) => {
    const {authorization} = req.headers
    // authorization === Bearer alkdjfakd
    if(!authorization) {
        return res.status(401).json({
            msg:"you must be logged in ",
            status:400
        })
    }
    const token = authorization.replace("Bearer ","")
    jwt.verify(token,JWT_SECRET,(err,payload) => {
        if(err) {
            return res.status(401).json({
                msg:"you must be logged in"
            })
        }

        const {_id} = payload
        UserModel.findById(_id)
        .then(userData=> {
            req.user = userData
            next()
        })
    })
}