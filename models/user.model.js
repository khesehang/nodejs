const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    firstname:{
        type: String,
    },
    lastname:String,
    username:{
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: Number, // 1 for admin, 2 for normal user and 3 for visition
        default: 2
    },
    status: {
        type: String,
        enum:['Active','inActive'],
        default: 'Active'
    }
},{
    timestamps: true
})

const UserModel = mongoose.model('user',UserSchema)

module.exports = UserModel;