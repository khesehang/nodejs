const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema.Types

const postSchema = new mongoose.Schema({
    title: {
        type: String,
        requried: true
    },
    body: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        // required: true
    },
    likes: [{type:ObjectId,ref:"user"}],
    postedBy: {
        type: ObjectId,
        ref: 'user'
    }
},{
    timestamps: true
})

const PostSchema = mongoose.model('post',postSchema)
module.exports = PostSchema