const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const UserModel = require('./../models/user.model')
const USER_MAP_REQ = require('./../helpers/user_map_req')
const passwordHash = require('password-hash')
const JWT = require('jsonwebtoken')
const keys = require('../keys')
const { URLSearchParams } = require('whatwg-url')

function generateToken(data) {
    let token = JWT.sign({
        _id: data._id
    },keys.JWT_SECRET)
    return token;
}

router.post('/signup',(req,res,next) => {
    const data  = req.body
    const newUser = new UserModel({})
    const newMapUser = USER_MAP_REQ(newUser,data)

    // password-hash
    newMapUser.password = passwordHash.generate(data.password)
    console.log('hashed password ',newMapUser.password)
    newMapUser.save((err,saved)=> {
        if(err) {
            console.log('error in signup')
            return next(err)
        }
        res.json(saved)
    })
    
})

router.post('/login',(req,res,next) => {
    const data = req.body
    UserModel.findOne({
        $or:[
            {
                username:req.body.username
            }, {
                email: data.username
            }
        ]
    })
    .then(user=> {
        if(!user) {
            return next({
                msg:"Invalid Username or Password",
                status:400
            })
        }
        // hashed password varificathin
        const isMatched = passwordHash.verify(data.password,user.password)
        if(!isMatched) {
            return next({
                msg:"Invalid Username or Password",
                status: 400
            })
        }

        // is active user
        if(user.status === 'inActive') {
            return next({
                msg: "Your account is disabled please contact system administrator for support",
                status: 403
            })
        }
        // token generation
        var token = generateToken(user)
        
        res.json({
            user: user,
            token: token
        })
    })
    .catch(err=> {
        next(err)
    })
})

module.exports = router