const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const PostSchema = require('../models/post.model')
POST_MAP_REQ = require('../helpers/post_map_req')

router.get('/allpost',(req,res,next)=> {
    const data = req.body
    console.log('req.body in allpost',data)
    PostSchema.find()
    .populate("postedBy","_id name")
    .then(posts => {
        res.json({posts})
    })
    .catch(err=> {
        return next(err)
    })
})

router.post('/createpost',(req,res,next) => {
    const data=req.body
    console.log('req.body in createpost',data)
    const newPost = new PostSchema({})
    const newPostMap = POST_MAP_REQ(newPost, data)
    newPost.save()
    .then(postData=> {
        console.log('create post successful')
        res.json(postData)
    })
    .catch(err=> {
        return next(err)
    })
})

router.get('/mypost',(req,res,next) => {
    const data = req.body
    console.log('req.body in mypost ',data)
    PostSchema.find({postedBy:data._id})
    .then(mypost => {
        return res.json(mypost)
    })
    .catch(err=> {
        return next(err)
    })
})

router.put('/like',(req,res,next)=> {
    const data = req.body
    console.log("req.data in like",data)
    PostSchema.findByIdAndUpdate(data.postId,{
        $push:{likes:req.user._id}
    },{
        new: true
    }).exec((err,result)=> {
        if(err) {
            return next(err)
        }else{
            res.json(result)
        }
    })
})

router.put("/unlike",(req,res,next)=> {
    PostSchema.findByIdAndUpdate(req.body._id,{
        $push: {likes:req.user._id}
    },{
        new: true
    }).exec((err,result) => {
        if(err) {
            return next(err)
        }
        res.json(result)
    })
})


module.exports = router;