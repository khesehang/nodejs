const router = require('express').Router()

// import middlewares
const requireLogin = require('./middleware/requireLogin')

// import routing level meddleware
const AuthRouter = require('./routes/auth.route')
const PostRouter = require('./routes/post.route')

router.use('/auth',AuthRouter)
router.use('/post',requireLogin,PostRouter)

module.exports  = router;