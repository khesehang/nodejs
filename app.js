const express = require('express')
const { default: mongoose, trusted } = require('mongoose')
const app = express()
const PORT = 8000
const {MONGOURI} = require('./keys')
const morgan = require('morgan')

const appRouting = require('./app.routing')

// for database entry
app.use(morgan('dev'))


app.use(express.urlencoded({
    extended: trusted
}))
app.use(express.json())


app.use('/api',appRouting)

mongoose.connect(MONGOURI)
mongoose.connection.on('connected',() => {
    console.log("mongodb connection successfull")
})
mongoose.connection.on('error',(err)=> {
    console.log('mongodb connection failed',err)
})

// applicational level error handler middleware
app.use((req,res,next) => {
    next({
        msg:"Not Found",
        status: 404
    })
})

app.use((error,req,res,next)=> {
    console.log('at error handling middleware is',error)
    res.status(error.status || 400)
    res.json({
        msg:error.msg || error,
        status: error.status || 400
    })
})

app.listen(PORT , () => {
    console.log("Server is running on PORT ",PORT)
})